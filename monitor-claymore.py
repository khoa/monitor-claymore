import sys
import os
from os import listdir
from os.path import isfile, join
import glob
from time import sleep

import os
import psutil
import signal
import random

def find_procs_by_name(name):
    "Return a list of processes matching 'name'."
    assert name, name
    ls = []
    for p in psutil.process_iter():
        name_, exe, cmdline = "", "", []
        try:
            name_ = p.name()
            cmdline = p.cmdline()
            exe = p.exe()
        except (psutil.AccessDenied, psutil.ZombieProcess):
            pass
        except psutil.NoSuchProcess:
            continue        
        if name == name_ or len(cmdline)>0 and cmdline[0] == name or os.path.basename(exe) == name:
            return p
    return None

folder = "C:/Users/1/Downloads/claymore 9.3/claymore 9.3/*_log.txt"
ether_name = "C:/Users/1/Downloads/claymore 9.3/claymore 9.3/EthDcrMiner64.exe"
pattern = "POOL/SOLO"

#folder = "C:/Users/1/Downloads/claymore 9.3/claymore 9.3/*.*"
#wait for the log file to be created first
sleep(10)
random.seed()
log_files = glob.glob(folder)
last_m_time = 0
last_m_file = None
for log_file in log_files:
    m_time = os.stat(log_file).st_mtime
    if m_time > last_m_time:
        last_m_time = m_time
        last_m_file = log_file
print last_m_file
print last_m_time
#wait for wait time and get the last line
def get_last_line(wait_time):
    global last_m_time
    print "waiting for ", wait_time
    sleep(wait_time)
    last_line = ''
    with open(last_m_file, 'r') as f:
        #get to the last line
        for line in f:
            pass
        last_line = line
    print last_line
    return last_line
while True:
    first_line = get_last_line(random.randint(5, 15))
    second_line = get_last_line(random.randint(5, 15))
    third_line = get_last_line(random.randint(5, 15))
    if first_line == second_line and first_line == third_line:
        process = find_procs_by_name('EthDcrMiner64.exe')
        if process != None:
            print 'Killing process', process.pid
            os.kill(int(process.pid), signal.SIGTERM)
            print last_line
            sys.exit(0)
